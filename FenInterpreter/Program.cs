﻿using FenInterpreter.FenBoard;
using FenInterpreter.FenPiece;

/**
Author: Maxwell Weinzierl
Project: FEN parser for chess
Email: maw150130@utdallas.edu
**/

using System;

namespace FenInterpreter
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //Some example FEN:
            //rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1
            //rnbqkbnr/ppp1pppp/3p4/1B6/4P3/8/PPPP1PPP/RNBQK1NR b KQkq - 1 2
            //rnbq1bnr/ppp4p/3kP3/6p1/2pP1K2/8/PPP2PPP/RNB2BNR w - g6 0 10

            //Helpful website to test program:
            //http://www.apronus.com/chess/wbeditor.php

            //Also no error handling since I assume you are putting in FEN correctly formatted

            /*
            Notes:

            */

            Console.WriteLine("Paste FEN here to determine if either side is in check:");
            string line;
            while ((line = Console.ReadLine()) != string.Empty)
            {
                var manager = new FenManager();
                manager.Open(line);
                //manager.Open("rn3b1r/ppp2q1p/3kP3/3R2p1/P7/4K3/1PP2PPP/1NB3NR b - - 0 23");
                //Paste in console or open through here

                Console.WriteLine($"White Check: {manager.IsCheck(FenSide.White)}");
                Console.WriteLine($"Black Check: {manager.IsCheck(FenSide.Black)}");
            };
        }
    }
}