﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FenInterpreter
{
    public class FenManager
    {
        private readonly string _fen;

        public FenBoard BoardObj { get; }

        public FenManager(string fen)
        {
            _fen = fen;
            BoardObj = new FenBoard(_fen);
        }

        public bool IsCheck()
        {
            return false;
        }
    }

    public struct FenLocation
    {
        public FenLocation(int y, int x)
        {
            Y = y;
            X = x;
        }

        public int Y { get; }
        public int X { get; }
    }

    public enum FenPieceType
    {
        Empty = '\0',
        Pawn = 'p',
        Knight = 'n',
        Bishop = 'b',
        Rook = 'r',
        Queen = 'q',
        King = 'k',
    }

    public class FenPiece
    {
        public FenPiece(FenPieceType type, FenLocation location)
        {
            Type = type;
            Location = location;
        }

        public FenPieceType Type { get; }
        public FenLocation Location { get; }
    }

    public class FenSide
    {
        public FenSide(List<FenPiece> pieces)
        {
            Pieces = pieces;
        }

        public List<FenPiece> Pieces { get; }
    }

    public class FenBoard
    {
        public FenBoard(string fen)
        {
            SetupBoard(fen);
        }

        public char[,] BoardArray { get; private set; }
        public FenSide WhiteSide { get; private set; }
        public FenSide BlackSide { get; private set; }

        private void SetupBoard(string fen)
        {
            BoardArray = new char[8, 8];

            var initial = fen.Substring(0, fen.IndexOf(' '));

            var lines = initial.Split('/');
            var whitePieces = new List<FenPiece>();
            var blackPieces = new List<FenPiece>();

            var y = 0;
            foreach (var line in lines)
            {
                var x = 0;
                foreach (var chr in line)
                {
                    if (char.IsDigit(chr))
                    {
                        x += (int)char.GetNumericValue(chr);
                    }
                    else
                    {
                        BoardArray[y, x] = chr;
                        var piece = new FenPiece((FenPieceType)char.ToLower(chr), new FenLocation(y, x));
                        if (char.IsUpper(chr))
                        {
                            whitePieces.Add(piece);
                        }
                        else
                        {
                            blackPieces.Add(piece);
                        }

                        x++;
                    }
                }
                y++;
            }
            WhiteSide = new FenSide(whitePieces);
            BlackSide = new FenSide(blackPieces);
        }
    }

    internal class Program
    {
        private static void Main(string[] args)
        {
            var manager = new FenManager("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
        }
    }
}