using FenInterpreter.FenPiece;
using FenInterpreter.FenPiece.FenTypes;
using System.Linq;

namespace FenInterpreter.FenBoard
{
    public class FenManager
    {
        private string _fen;

        public FenBoard BoardObj { get; private set; }

        public bool IsCheck(FenSide side)
        {
            var kingLoc = BoardObj.Pieces.First(x => x.Side == side && x is King).Location;//TODO Throws error if no king is found

            return BoardObj.Pieces.Where(x => x.Side != side).Any(piece => piece.AttackMoves.Contains(kingLoc));
        }

        public void Open(string fen)
        {
            _fen = fen;
            BoardObj = new FenBoard(_fen);
        }
    }
}