using FenInterpreter.FenPiece;
using FenInterpreter.FenPiece.FenTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace FenInterpreter.FenBoard
{
    public class FenBoard
    {
        public List<FenPiece.FenPiece> Pieces { get; private set; }

        public FenBoard(string fen)
        {
            SetupBoard(fen);
            CalculateMoves();
        }

        private void CalculateMoves()
        {
            Pieces.ForEach(x => x.GetPotentialMoves(Pieces));
        }

        private void SetupBoard(string fen)
        {
            var boardState = fen.Trim();
            if (boardState.Contains(' '))
                boardState = boardState.Substring(0, fen.IndexOf(' '));
            var boardLines = boardState.Split('/').ToList();

            var newPieces = new List<FenPiece.FenPiece>();

            var y = 0;

            foreach (var line in boardLines)
            {
                var x = 0;
                foreach (var pieceChar in line)
                {
                    if (char.IsDigit(pieceChar))
                    {
                        x += (int)char.GetNumericValue(pieceChar);
                    }
                    else
                    {
                        newPieces.Add(CreatePiece(((FenPieceType)char.ToLower(pieceChar)),
                            char.IsUpper(pieceChar) ? FenSide.White : FenSide.Black, (new FenLocation(y, x))));
                        x++;
                    }
                }
                y++;
            }
            Pieces = newPieces;
        }

        public FenPiece.FenPiece CreatePiece(FenPieceType type, FenSide side, FenLocation location)
        {
            switch (type)
            {
                case FenPieceType.Empty:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);

                case FenPieceType.Pawn:
                    return new Pawn(side, location);

                case FenPieceType.Knight:
                    return new Knight(side, location);

                case FenPieceType.Bishop:
                    return new Bishop(side, location);

                case FenPieceType.Rook:
                    return new Rook(side, location);

                case FenPieceType.Queen:
                    return new Queen(side, location);

                case FenPieceType.King:
                    return new King(side, location);

                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
    }
}