namespace FenInterpreter.FenPiece
{
    public enum FenSide
    {
        White = 0,
        Black = 1,
    }
}