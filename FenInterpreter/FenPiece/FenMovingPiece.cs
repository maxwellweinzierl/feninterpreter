using System.Collections.Generic;
using System.Linq;

namespace FenInterpreter.FenPiece
{
    internal abstract class FenMultipleMovePiece : FenPiece
    {
        protected FenMultipleMovePiece(FenSide side, FenLocation location) : base(side, location)
        {
        }

        public override void GetPotentialMoves(List<FenPiece> pieces)
        {
            var baseMoves = MoveList;
            var newMoves = new List<FenLocation>();
            if (Side == FenSide.Black)
                baseMoves = baseMoves.Select(location => new FenLocation(-1 * location.Y, location.X)).ToList();

            foreach (var move in baseMoves)
            {
                for (var i = 1; i < 8; i++)
                {
                    var newLoc = Location + (move * i);
                    if (newLoc.Y > 7 || newLoc.Y < 0 || newLoc.X > 7 || newLoc.X < 0)
                        break;

                    if (pieces.Select(piece => piece.Location).Contains(newLoc) && pieces.First(piece => piece.Location == newLoc).Side == Side)
                    {
                        if (pieces.First(piece => piece.Location == newLoc).Side != Side)
                        {
                            newMoves.Add(newLoc);
                        }
                        break;
                    }
                    newMoves.Add(newLoc);
                }
            }

            AttackMoves = newMoves;
        }
    }

    internal abstract class FenSingleMovePiece : FenPiece
    {
        protected FenSingleMovePiece(FenSide side, FenLocation location) : base(side, location)
        {
        }

        public override void GetPotentialMoves(List<FenPiece> pieces)
        {
            var baseMoves = MoveList;
            if (Side == FenSide.Black)
                baseMoves = baseMoves.Select(x => new FenLocation(-1 * x.Y, x.X)).ToList();

            var newMoves = baseMoves.Select(move => Location + (move)).Where(newLoc => newLoc.Y <= 7 && newLoc.Y >= 0 && newLoc.X <= 7 && newLoc.X >= 0).Where(newLoc => !pieces.Select(z => z.Location).Contains(newLoc) || pieces.First(y => y.Location == newLoc).Side != Side).ToList();

            AttackMoves = newMoves;
        }
    }
}