namespace FenInterpreter.FenPiece
{
    public struct FenLocation
    {
        public int Y { get; }

        public int X { get; }

        public FenLocation(int y, int x)
        {
            Y = y;
            X = x;
        }

        public static bool operator !=(FenLocation l1, FenLocation l2)
        {
            return l1.Y != l2.Y && l1.X != l2.X;
        }

        public static FenLocation operator *(FenLocation l1, FenLocation l2)
        {
            return new FenLocation(l1.Y * l2.Y, l1.X * l2.X);
        }

        public static FenLocation operator *(FenLocation l1, int l2)
        {
            return new FenLocation(l1.Y * l2, l1.X * l2);
        }

        public static FenLocation operator +(FenLocation l1, FenLocation l2)
        {
            return new FenLocation(l1.Y + l2.Y, l1.X + l2.X);
        }

        public static bool operator ==(FenLocation l1, FenLocation l2)
        {
            return l1.Y == l2.Y && l1.X == l2.X;
        }

        public bool Equals(FenLocation other)
        {
            return Y == other.Y && X == other.X;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is FenLocation && Equals((FenLocation)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Y * 397) ^ X;
            }
        }

        public override string ToString()
        {
            return $"({Y},{X})";
        }
    }
}