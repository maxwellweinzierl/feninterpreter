namespace FenInterpreter.FenPiece
{
    public enum FenPieceType
    {
        Empty = '\0',
        Pawn = 'p',
        Knight = 'n',
        Bishop = 'b',
        Rook = 'r',
        Queen = 'q',
        King = 'k',
    }
}