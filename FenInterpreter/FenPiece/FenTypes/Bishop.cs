﻿using System.Collections.Generic;

namespace FenInterpreter.FenPiece.FenTypes
{
    internal class Bishop : FenMultipleMovePiece
    {
        private static readonly List<FenLocation> Moves = new List<FenLocation>
        {
            new FenLocation(-1, -1), new FenLocation(1, 1), new FenLocation(-1, 1), new FenLocation(1, -1),
        };

        protected override List<FenLocation> MoveList => Moves;

        public Bishop(FenSide side, FenLocation location) : base(side, location)
        {
        }
    }
}