﻿using System.Collections.Generic;

namespace FenInterpreter.FenPiece.FenTypes
{
    internal class Queen : FenMultipleMovePiece
    {
        private static readonly List<FenLocation> Moves = new List<FenLocation>
        {
            new FenLocation(1, 0), new FenLocation(-1, 0), new FenLocation(0, 1), new FenLocation(0, -1),
            new FenLocation(-1, -1), new FenLocation(1, 1), new FenLocation(-1, 1), new FenLocation(1, -1),
        };

        protected override List<FenLocation> MoveList => Moves;

        public Queen(FenSide side, FenLocation location) : base(side, location)
        {
        }
    }
}