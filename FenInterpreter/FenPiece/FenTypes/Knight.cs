﻿using System.Collections.Generic;

namespace FenInterpreter.FenPiece.FenTypes
{
    internal class Knight : FenSingleMovePiece
    {
        private static readonly List<FenLocation> Moves = new List<FenLocation>
        {
            new FenLocation(-2, 1), new FenLocation(2, 1),
            new FenLocation(-1, 2), new FenLocation(1, 2),
            new FenLocation(2, -1), new FenLocation(-2, -1),
            new FenLocation(1, -2), new FenLocation(-1, -2),
        };

        protected override List<FenLocation> MoveList => Moves;

        public Knight(FenSide side, FenLocation location) : base(side, location)
        {
        }
    }
}