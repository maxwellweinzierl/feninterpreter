﻿using System.Collections.Generic;

namespace FenInterpreter.FenPiece.FenTypes
{
    internal class Pawn : FenSingleMovePiece
    {
        private static readonly List<FenLocation> Moves = new List<FenLocation>
        {
            new FenLocation(-1, 1),
            new FenLocation(-1, -1),
        };

        protected override List<FenLocation> MoveList => Moves;

        public Pawn(FenSide side, FenLocation location) : base(side, location)
        {
        }
    }
}