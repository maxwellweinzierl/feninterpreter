using System.Collections.Generic;
using System.Linq;

namespace FenInterpreter.FenPiece
{
    public abstract class FenPiece
    {
        //private static readonly Dictionary<FenPieceType, List<FenLocation>> MoveDictionary = new Dictionary<FenPieceType, List<FenLocation>>()
        //{
        //    {
        //        FenPieceType.Pawn, new List<FenLocation>()
        //        {
        //            new FenLocation(-1, 1), new FenLocation(-1, -1),
        //        }},
        //    {
        //        FenPieceType.Knight, new List<FenLocation>()
        //        {
        //            new FenLocation(-2, 1), new FenLocation(2, 1),
        //            new FenLocation(-1, 2), new FenLocation(1, 2),
        //            new FenLocation(2, -1), new FenLocation(-2, -1),
        //            new FenLocation(1, -2), new FenLocation(-1, -2),
        //        }},
        //    {
        //        FenPieceType.Bishop, new List<FenLocation>()
        //        {
        //            new FenLocation(-1, -1), new FenLocation(1, 1), new FenLocation(-1, 1), new FenLocation(1, -1),
        //        }},
        //    {
        //        FenPieceType.Rook, new List<FenLocation>()
        //        {
        //            new FenLocation(1, 0), new FenLocation(-1, 0), new FenLocation(0, 1), new FenLocation(0, -1),
        //        }},
        //    {
        //        FenPieceType.Queen, new List<FenLocation>()
        //        {
        //            new FenLocation(1, 0), new FenLocation(-1, 0), new FenLocation(0, 1), new FenLocation(0, -1),
        //            new FenLocation(-1, -1), new FenLocation(1, 1), new FenLocation(-1, 1), new FenLocation(1, -1),
        //        }},
        //    {
        //        FenPieceType.King, new List<FenLocation>()
        //        {
        //            new FenLocation(1, 0), new FenLocation(-1, 0), new FenLocation(0, 1), new FenLocation(0, -1),
        //            new FenLocation(-1, -1), new FenLocation(1, 1), new FenLocation(-1, 1), new FenLocation(1, -1),
        //        }},
        //};

        protected abstract List<FenLocation> MoveList { get; }

        public List<FenLocation> AttackMoves { get; protected set; }
        public FenLocation Location { get; }
        public FenSide Side { get; }

        protected FenPiece(FenSide side, FenLocation location)
        {
            Side = side;
            Location = location;
        }

        public abstract void GetPotentialMoves(List<FenPiece> pieces);

        public override string ToString()
        {
            return $"{Side} {GetType().Name}: {Location}";
        }

        //public void GetPotentialMoves(List<FenPiece> pieces)
        //{
        //    var baseMoves = MoveDictionary[Type];
        //    var newMoves = new List<FenLocation>();
        //    if (Side == FenSide.Black && Type == FenPieceType.Pawn)
        //        baseMoves = baseMoves.Select(x => new FenLocation(-1 * x.Y, x.X)).ToList();
        //    var max = 2;
        //    if (Type != FenPieceType.Pawn && Type != FenPieceType.Knight && Type != FenPieceType.King)
        //        max = 8;

        //    baseMoves.ForEach(x =>
        //    {
        //        for (var i = 1; i < max; i++)
        //        {
        //            var newLoc = Location + (x * i);
        //            if (newLoc.Y > 7 || newLoc.Y < 0 || newLoc.X > 7 || newLoc.X < 0)
        //                break;

        //            if (pieces.Select(z => z.Location).Contains(newLoc))
        //            {
        //                if (pieces.First(y => y.Location == newLoc).Side != Side)
        //                {
        //                    newMoves.Add(newLoc);
        //                }
        //                break;
        //            }
        //            newMoves.Add(newLoc);
        //        }
        //    });

        //    AttackMoves = newMoves;
        //}
    }
}